<?php

    do
    {
        printf("Enter a number: ");
        if (($r = fgets(STDIN)) == false)
        {
            echo "^D\n";
            break ;
        }
        $r = trim($r);
        if (is_numeric($r))
            printf("The number %s is %s\n", $r, ($r % 2 == 0) ? "even" : "odd");
        else
            printf("'%s' is not a number\n", $r);
    }
	while (true);
