<?php
    function ft_split(string $str): array
    {
        $arr = preg_split('/\s+/', trim($str));
        sort($arr, SORT_STRING);
        return ($arr);
    }
