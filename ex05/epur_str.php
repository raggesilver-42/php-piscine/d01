<?php
    function epur_str(string $str)
    {
        $arr = preg_split('/\s+/', trim($str));
        $str = join(" ", $arr);
        return ($str);
    }

    if ($argc == 2)
        printf("%s\n", epur_str($argv[1]));
