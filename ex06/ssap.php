<?php
    array_shift($argv);
    $argc--;

    if ($argc > 0)
    {
        $arr = array();
        foreach ($argv as $v)
            foreach (preg_split('/\s+/', trim($v)) as $vv)
                array_push($arr, $vv);
        sort($arr, SORT_STRING);
        foreach ($arr as $v)
            printf("%s\n", $v);
    }
