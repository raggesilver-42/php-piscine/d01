<?php

    function rostring(string $str): string
    {
        $arr = preg_split('/\s+/', trim($str));
        $len = count($arr);
        if ($len > 1)
        {
            $tmp = $arr[0];
            array_shift($arr);
            array_push($arr, $tmp);
        }
        return (join(" ", $arr));
    }

    if ($argc > 1)
        printf("%s\n", rostring($argv[1]));
