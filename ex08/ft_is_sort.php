<?php
    function ft_is_sort(array $tab): bool
    {
        $len = sizeof($tab);
        for ($i = 0; $i < $len; $i++)
            for ($j = $i + 1; $j < $len; $j++)
            {
                if (strcmp($tab[$i], $tab[$j]) > 0)
                    return (false);
            }
        return (true);
    }
