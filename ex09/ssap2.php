<?php
    function get_char_type($c): int
    {
        if (($c >= 'a' && $c <= 'z') || ($c >= 'A' && $c <= 'Z'))
            return (0);
        if ($c >= '0' && $c <= '9')
            return (1);
        return (2);
    }

    function ssap2_sort(string $_s1, string $_s2): int
    {
        $s1  = str_split($_s1);
        $s2  = str_split($_s2);
        $i   = 0;
        $ret;

        while ($i < sizeof($s1) && $i < sizeof($s2))
        {
            if (($ret = strcasecmp($s1[$i], $s2[$i])) != 0)
                break ;
            $i++;
        }

        if ($ret != 0)
        {
            $c1 = get_char_type($s1[$i]);
            $c2 = get_char_type($s2[$i]);
            if ($c1 != $c2)
                return (($c1 < $c2) ? -1 : 1);
            return ($ret);
        }
        // If the characters are equal the while loop stopped because of length
        // difference. Return whether $s1 is shorter than $s2
        else
        {
            return ((sizeof($s1) == $i) ? -1 : 1);
        }
    }

    array_shift($argv);
    $argc--;

    if ($argc > 0)
    {
        $arr = array();
        foreach ($argv as $v)
            foreach (preg_split('/\s+/', trim($v)) as $vv)
                array_push($arr, $vv);
        usort($arr, "ssap2_sort");
        foreach ($arr as $v)
            printf("%s\n", $v);
    }
