<?php
    if ($argc != 4)
    {
        printf("Incorrect Parameters\n");
        exit(1);
    }

    array_shift($argv);
    $p1 = intval(trim($argv[0]));
    $p2 = intval(trim($argv[2]));
    $op = trim($argv[1]);

    switch ($op)
    {
        case "+":
            printf("%d\n", $p1 + $p2);
            break;
        case "-":
            printf("%d\n", $p1 - $p2);
            break;
        case "*":
            printf("%d\n", $p1 * $p2);
            break;
        case "/":
            printf("%d\n", $p1 / $p2);
            break;
        case "%":
            printf("%d\n", $p1 % $p2);
            break;
    }
