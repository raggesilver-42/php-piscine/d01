<?php

    function ft_die(string $msg, int $status)
    {
        printf("%s\n", $msg);
        exit($status);
    }

    function ft_eval(string $str): float
    {
        $str = join("", preg_split('/\s+/', trim($str)));

        $reg = '/^(\-*?\d+(?:\.\d+)?)([-+*%\/])(\-*?\d+(?:\.\d+)?)$/';
        if (!preg_match($reg, $str))
            ft_die("Syntax Error", 1);

        $arr = array();
        preg_match_all($reg, $str, $arr);

        array_shift($arr); // Remove the full match result

        // To see why $arr[x][0] to get the value uncomment next line
        // print_r($arr);

        $a = floatval($arr[0][0]);
        $b = floatval($arr[2][0]);
        $res = 0.0;

        switch($arr[1][0])
        {
            case '+':
                $res = $a + $b;
                break ;
            case '-':
                $res = $a - $b;
                break ;
            case '*':
                $res = $a * $b;
                break ;
            case '/':
                $res = $a / $b;
                break ;
            case '%':
                $res = $a % $b;
                break ;
            default:
                ft_die("Syntax Error", 1);
        }

        return ($res);
    }

    if ($argc != 2)
        ft_die("Incorrect Parameters", 1);

    echo ft_eval($argv[1]) . "\n";
