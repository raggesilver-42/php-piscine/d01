<?php

    if ($argc > 2)
    {
        $find = $argv[1];

        $arr = array();

        for ($i = 2; $i < $argc; $i++)
        {
            $ar = explode(":", $argv[$i]);
            $arr[$ar[0]] = $ar[1];
        }

        if (array_key_exists($find, $arr))
        {
            echo $arr[$find] . "\n";
        }
    }
